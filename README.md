# Images

Various build images used by pipelines.

## Architecture

This project consists of sub folders which house Dockerfiles with the required tweaks for pipeline use. Each folder
should be named after the image it builds. A `.env` file is executed to get additional environment variables. The system
expects at least `VERSION` to be exported.
