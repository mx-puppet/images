#!/bin/sh

cat <<EOH > ci.yml
stages:
  - prepare
  - build
EOH

for DIR in */Dockerfile
do
  export IMAGE=${DIR%/Dockerfile}
  printf "%s" "Found: $IMAGE"
  while read -r VERSION
  do
    printf "%s" " $VERSION"
    export VERSION
    envsubst "\${IMAGE} \${VERSION}" < docker.gitlab-ci.yml >> ci.yml
  done < "${IMAGE}/VERSIONS"
  echo
done
